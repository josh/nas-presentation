\documentclass[usenames, dvipsnames]{beamer}

\setbeamertemplate{caption}[numbered]
\usepackage[utf8]{inputenx} % For æ, ø, å
\usepackage{csquotes}       % Quotation marks
\usepackage{microtype}      % Improved typography
\usepackage{amssymb}        % Mathematical symbols
\usepackage{mathtools}      % Mathematical symbols
\usepackage[absolute, overlay]{textpos} % Arbitrary placement
\setlength{\TPHorizModule}{\paperwidth} % Textpos units
\setlength{\TPVertModule}{\paperheight} % Textpos units
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
\usetikzlibrary{overlay-beamer-styles}  % Overlay effects for TikZ
\usetikzlibrary{positioning}

\usetheme{UiB}

\nologo
\author{Joshua Karns}
\title{Neural Architecture Search}
\subtitle{A survey of modern approaches to NAS}

\tikzset{
%Define standard arrow tip
>=stealth',
%Define style for boxes
normalNode/.style={
       rectangle,
       rounded corners,
       fill=yellow!20,
       draw=black, very thick,
       minimum width=2em,
       minimum height=2em,
       text centered},
inputNode/.style={
       rectangle,
       rounded corners,
       fill=Apricot,
       draw=black, very thick,
       minimum width=2em,
       minimum height=2em,
       text centered},
outputNode/.style={
       rectangle,
       rounded corners,
       fill=Bittersweet,
       draw=black, very thick,
       minimum width=2em,
       minimum height=2em,
       text centered},
% Define arrow style
pil/.style={
       ->,
       thick,
       shorten <=2pt,
       shorten >=2pt,}
}

\begin{document}

\section{What is Neural Architecture Search?}
\subsection{AutoML}

\begin{frame}
    \frametitle{AutoML}

    \begin{question}
        What is AutoML?
    \end{question}
    
    \pause
    
    Automating the entire machine learning process:
    
    \pause

    \begin{enumerate}[<+-| alert@+>]
        \item Data preparation
        \item Feature engineering
        \item Model selection
        \item All with a reasonable run time
    \end{enumerate}
    \pause 
    The part of AutoML neural architecture search is concerned with is model selection:
    \begin{itemize}[<+-| alert@+>]
        \item A model is defined in terms of its hyperparameters (e.g learning rate, number of layers, activation function, optimization method, etc.)
    \end{itemize}

\end{frame}

\subsection{Model Selection}

\begin{frame}
    \frametitle{Model Selection - One Definition}
    
    \footnotesize
    \begin{definition}
        \begin{itemize}[<+-| alert@+>]
            \item Let $\mathcal{A}$ be an ML algorithm with $\mathcal{N}$ hyperparameters.
            \item Let $\Lambda_n$ be the domain of the $n^{th}$ hyperparameter.
            \item Then, the configuration space of all models is: $\Lambda = \Lambda_1 \times \Lambda_2 \times ... \times \Lambda_{\mathcal{N}} $
            \item This configuration space contains contingencies.
            \item Arbitrary configuration $\lambda$ is a subset of $\Lambda$.
            \item Let $\mathcal{A}_\lambda$ denote ML algorithm $\mathcal{A}$ instantiated with hyperparameters $\lambda$.
            \item We can then define Model Selection to be the process of finding $\lambda^*$ such that
            $$\lambda^* = \underset{\lambda \in \Lambda}{\text{argmin}} \quad V(\mathcal{L}, \mathcal{A}_\lambda, D_{train}, D_{test}) $$
            for loss function $\mathcal{L}$, validation protocol $V$, and $(D_{train}, D_{test}) \sim \mathcal{D}$
            \item This is not the only way to view Model Selection!
        \end{itemize}
    \end{definition}
    
\end{frame}

\subsection{NAS}

\begin{frame}
    \frametitle{Neural Architecture Search}
    
    \begin{itemize}[<+-| alert@+>]
        \item Neural Architecture Search (NAS) is only concerned with a subset of the hyperparameters for Model Selection.
            \begin{itemize}
                \item i.e. $\Lambda^{arch} = \Lambda_{a_1} \times \Lambda_{a_2} \times ... \times \Lambda_{a_\mathcal{M}}$ \\
                    where $a_i$ is the $i^{th}$ hyperparameter concerned with neural architecture,
                    for a total of $\mathcal{M}$ architecture related hyperparameters.
            \end{itemize}
        \item A NAS can be considered one part of a complete of Model Selection algorithm.
    \end{itemize}
\end{frame}
\subsection{Components of NAS}
\begin{frame}
    \frametitle{Components of NAS}
      
    \begin{figure}
    \centering
    \begin{tikzpicture}[node distance=2.5cm]
        \tikzstyle{every state}=[fill=red,draw=none,text=white]
        \node[normalNode] (searchSpace) {Search Space};
            %edge[pil, bend right=45] (searchStrat.west);
        \node[normalNode, right=1.5cm of searchSpace] (searchStrat) {Search Strategy};
        \node[normalNode, right=1.5cm of searchStrat] (perfEstStrat) {\tiny Performance Estimation Strategy};
        
        \path[thick, ->] (searchSpace) edge node [above] 
            {$\lambda_{arch}$} (searchStrat);
        \path[thick, ->] (searchStrat.north) edge[bend left=45, above] node 
            {$\mathcal{A}_{\lambda_{arch}}$} (perfEstStrat.north);
        \path[thick, ->] (perfEstStrat.south) edge[bend left=45, above] node [below]
            {performance estimation $\approx$ error} (searchStrat.south);
    \end{tikzpicture}
    \caption{How a NAS algorithm function at a very high level.}
    \label{fig:nas}
    \end{figure}
    
    \pause

    \begin{itemize}
        \item These three components are where approaches to NAS differ the most
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Search Space}
    \begin{itemize}
        \item Defines what neural architectures are possible.
        \item Prior knowledge can be introduced, but can cause bias.
        \item Search spaces can vary widely in their complexity.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Search Space - Simple Chains}

    \begin{itemize}
        \item The simplest search space we will discuss.
        \item Can be parameterized by:
            \begin{itemize}
                \item Number of layers 
                \item Operations performed by each layer (pooling, convolutions, etc.)
                \item Activation functions
            \end{itemize}
        \item Smallest non-trivial search space.
    \end{itemize}

    \begin{figure}
        \centering
        \begin{tikzpicture}[node distance=2.5cm]
            \tikzstyle{every state}=[fill=red,draw=none,text=white]
            \node[inputNode] (input) {Input};
            \node[normalNode, right=1cm of input] (l1) {L1};
            \node[normalNode, right=1cm of l1] (l2) {L2};
            \node[right=1cm of l2] (dot) {...};
            \node[normalNode, right=1cm of dot] (ln) {Ln};
            \node[outputNode, right=1cm of ln] (output) {Output};


            \path[thick, ->] (input.east) edge (l1.west);
            \path[thick, ->] (l1.east) edge (l2.west);
            \path[thick, ->] (l2.east) edge (dot.west);
            \path[thick, ->] (dot.east) edge (ln.west);
            \path[thick, ->] (ln.east) edge (output.west);
        \end{tikzpicture}
        \caption{The structure of a simple chain is the same as that of a linked list.}
        \label{fig:simple_chain}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Search Space - Complex Chains}

    \begin{itemize}
        \item Allows for more complex structures.
            \begin{itemize}
                \item Skip connections
                \item Multi-branch networks
            \end{itemize}
        \item Similar to search space of all DAGs, except each node has an
            operation type.
        \item Special cases of complex chains:
            \begin{itemize}
                \item Simple Chains
                \item Residual Networks \cite{resnets}
                \item DenseNets \cite{densenets}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Complex Chains - Basic Example}
    \begin{figure}
        \centering
        \begin{tikzpicture}[node distance=2.5cm]
            \tikzstyle{every state}=[fill=red,draw=none,text=white]
            \node[inputNode] (input) {Input};
            \node[normalNode, right=1cm of input] (l1) {L1};
            \node[normalNode, right=1cm of l1] (l2) {L2};
            \node[normalNode, above=1cm of l2] (l3) {L3};
            \node[normalNode, right=1cm of l2] (l4) {L4};
            \node[outputNode, right=1cm of l4] (output) {Output};


            \path[thick, ->] (l1.east) edge[bend right=45, above] (l4.west);
            \path[thick, ->] (input.east) edge (l1.west);
            \path[thick, ->] (l1.east) edge (l2.west);
            \path[thick, ->] (l1.east) edge (l3.west);
            \path[thick, ->] (l3.east) edge (l4.west);
            \path[thick, ->] (l2.east) edge (l4.west);
            \path[thick, ->] (l3.east) edge (l4.west);
            \path[thick, ->] (l4.east) edge (output.west);

        \end{tikzpicture}
        \caption{An example of a complex chain network architecture meant to highlight
                its differences from simple chains.}
        \label{fig:complex_chain}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Complex Chains - Some More Examples}
    \begin{figure}
        \centering
        \begin{tikzpicture}[node distance=2.5cm]
            \tikzstyle{every state}=[fill=red,draw=none,text=white]
            \node[inputNode] (input) {Input};
            \node[normalNode, right=1cm of input] (l1) {L1};
            \node[normalNode, right=1cm of l1] (l2) {L2};
            \node[normalNode, right=1cm of l2] (l3) {L3};
            \node[normalNode, right=1cm of l3] (l4) {L4};
            \node[outputNode, right=1cm of l4] (output) {Output};

            \path[thick, ->] (l1.east) edge[bend left=45, above] (l3.west);
            \path[thick, ->] (l1.east) edge[bend left=45, above] (l4.west);
            \path[thick, ->] (l2.east) edge[bend left=45, above] (l4.west);

            \path[thick, ->] (input.east) edge (l1.west);
            \path[thick, ->] (l1.east) edge (l2.west);
            \path[thick, ->] (l2.east) edge (l3.west);
            \path[thick, ->] (l3.east) edge (l4.west);
            \path[thick, ->] (l4.east) edge (output.west);

        \end{tikzpicture}
        \caption{A special case of complex chains, coined DenseNets by \cite{densenets}}
        \label{fig:densenets}
    \end{figure}

    \begin{figure}
        \centering
        \begin{tikzpicture}[node distance=2.5cm]
            \tikzstyle{every state}=[fill=red,draw=none,text=white]
            \node[inputNode] (input) {Input};
            \node[normalNode, right=1cm of input] (l1) {L1};
            \node[normalNode, right=1cm of l1] (l2) {L2};
            \node[normalNode, right=1cm of l2] (l3) {L3};
            \node[normalNode, right=1cm of l3] (l4) {L4};
            \node[outputNode, right=1cm of l4] (output) {Output};

            \path[thick, ->] (l1.east) edge[bend left=45, above] (l3.west);
            \path[thick, ->] (l2.east) edge[bend left=45, above] (l4.west);

            \path[thick, ->] (input.east) edge (l1.west);
            \path[thick, ->] (l1.east) edge (l2.west);
            \path[thick, ->] (l2.east) edge (l3.west);
            \path[thick, ->] (l3.east) edge (l4.west);
            \path[thick, ->] (l4.east) edge (output.west);

        \end{tikzpicture}
        \caption{Another special case, coined Residual networks by \cite{resnets}.}
        \label{fig:resnets}
    \end{figure}

    \begin{itemize}
        \item Both of these special cases are effective in addressing the disappearing / exploding
            gradient problem.
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Search Space - Motifs}

    \begin{itemize}
        \item Rather than searching for a complete neural architecture,
            search for small motifs which will be combined in a fixed way.
        \item Search space is smaller because motifs should have fewer layers
            than full networks.
        \item Search space for motifs can be any other search space! 
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Search Space - Motif Examples}
    \begin{figure}
        \centering

       \begin{minipage}{.4\textwidth}
            \raggedright
            \fcolorbox{BlueViolet}{SkyBlue}{
            \begin{tikzpicture}[node distance=2.5cm]
                \tikzstyle{every state}=[fill=red,draw=none,text=white]
                \node[inputNode] (x1) {$x_1$};
                \node[inputNode, right=0.5cm of x1] (x2) {$x_2$};
                \node[inputNode, right=0.5cm of x2] (x3) {$x_3$};
                \node[normalNode, below=0.5cm of x1] (h11) {$h_{1, 1}$};
                \node[normalNode, below=0.5cm of x2] (h12) {$h_{1, 2}$};
                \node[normalNode, below=0.5cm of x3] (h13) {$h_{1, 3}$};
                \node[normalNode, below=0.5cm of h11] (h21) {$h_{2, 1}$};
                \node[normalNode, below=0.5cm of h13] (h22) {$h_{2, 2}$};
                \node[outputNode, below=0.5cm of h21] (h31) {$y_1$};
                \node[outputNode, right=0.5cm of h31] (h32) {$y_2$};
                \node[outputNode, below=0.5cm of h22] (h33) {$y_3$};
                
                \path[thick, ->] (x1.south) edge (h11.north);
                \path[thick, ->] (x1.south) edge (h12.north);
                \path[thick, ->] (x1.south) edge (h13.north);
                \path[thick, ->] (x2.south) edge (h11.north);
                \path[thick, ->] (x2.south) edge (h12.north);
                \path[thick, ->] (x2.south) edge (h13.north);
                \path[thick, ->] (x3.south) edge (h11.north);
                \path[thick, ->] (x3.south) edge (h12.north);
                \path[thick, ->] (x3.south) edge (h13.north);
                
                \path[thick, ->] (h11.south) edge (h21.north);
                \path[thick, ->] (h12.south) edge (h32.north);
                \path[thick, ->] (h13.south) edge (h22.north);

                \path[thick, ->] (h11.south) edge (h33.north);
                \path[thick, ->] (h13.south) edge (h31.north);

                \path[thick, ->] (h21.south) edge (h31.north);
                \path[thick, ->] (h22.south) edge (h33.north);
                \path[thick, ->] (h21.south) edge (h32.north);
                \path[thick, ->] (h22.south) edge (h32.north);
                
            \end{tikzpicture}
            }
        \end{minipage}%
        \begin{minipage}{0.4\textwidth}
            \raggedleft
            \fcolorbox{BlueViolet}{Thistle}{
            \begin{tikzpicture}[node distance=2.5cm]
                \tikzstyle{every state}=[fill=red,draw=none,text=white]
                \node[inputNode] (x1) {$x_1$};
                \node[inputNode, right=0.5cm of x1] (x2) {$x_2$};
                \node[inputNode, right=0.5cm of x2] (x3) {$x_3$};
                \node[normalNode, below=0.5cm of x1] (h11) {$h_{1, 1}$};
                \node[normalNode, below=0.5cm of x2] (h12) {$h_{1, 2}$};
                \node[normalNode, below=0.5cm of x3] (h13) {$h_{1, 3}$};
                \node[normalNode, below=0.5cm of h11] (h21) {$h_{2, 1}$};
                \node[normalNode, below=0.5cm of h12] (h22) {$h_{2, 2}$};
                \node[normalNode, below=0.5cm of h13] (h23) {$h_{2, 3}$};
                \node[outputNode, below right=0.5cm and 0cm of h21] (h31) {$y_1$};
                \node[outputNode, right=0.5cm of h31] (h32) {$y_2$};
                
                \path[thick, ->] (x1.south) edge (h11.north);
                \path[thick, ->] (x1.south) edge (h12.north);
                \path[thick, ->] (x1.south) edge (h13.north);
                \path[thick, ->] (x2.south) edge (h11.north);
                \path[thick, ->] (x2.south) edge (h12.north);
                \path[thick, ->] (x2.south) edge (h13.north);
                \path[thick, ->] (x3.south) edge (h11.north);
                \path[thick, ->] (x3.south) edge (h12.north);
                \path[thick, ->] (x3.south) edge (h13.north);

                \path[thick, ->] (h11.south) edge (h21.north);
                \path[thick, ->] (h11.south) edge (h22.north);
                \path[thick, ->] (h11.south) edge (h23.north);
                \path[thick, ->] (h12.south) edge (h21.north);
                \path[thick, ->] (h12.south) edge (h22.north);
                \path[thick, ->] (h12.south) edge (h23.north);
                \path[thick, ->] (h13.south) edge (h21.north);
                \path[thick, ->] (h13.south) edge (h22.north);
                \path[thick, ->] (h13.south) edge (h23.north);

                \path[thick, ->] (h21.south) edge (h31.north);
                \path[thick, ->] (h22.south) edge (h31.north);
                \path[thick, ->] (h23.south) edge (h31.north);
                \path[thick, ->] (h21.south) edge (h32.north);
                \path[thick, ->] (h22.south) edge (h32.north);
                \path[thick, ->] (h23.south) edge (h32.north);
                               
            \end{tikzpicture}
            } 
        \end{minipage} 
        \caption{Two examples of possible motifs}
        \label{fig:nas}
    \end{figure}
    \centering

    \begin{figure}
        \centering
        \begin{tikzpicture}[node distance=2.5cm]
            \tikzstyle{every state}=[fill=red,draw=none,text=white]
            \node[inputNode] (x) {Input};
            \node[inputNode, fill=Thistle, right=0.5cm of x] (l1) {$L_1$};
            \node[inputNode, fill=SkyBlue, right=0.5cm of l1] (l2) {$L_2$};
            \node[inputNode, fill=SkyBlue, right=0.5cm of l2] (l3) {$L_3$};
            \node[inputNode, fill=Thistle, right=0.5cm of l3] (l4) {$L_4$};
            \node[inputNode, fill=Thistle, right=0.5cm of l4] (l5) {$L_5$};
            \node[outputNode, right=0.5cm of l5] (y) {Output};
            
            \path[thick, ->] (x.east) edge (l1.west);
            \path[thick, ->] (l1.east) edge (l2.west);
            \path[thick, ->] (l2.east) edge (l3.west);
            \path[thick, ->] (l3.east) edge (l4.west);
            \path[thick, ->] (l4.east) edge (l5.west);
            \path[thick, ->] (l5.east) edge (y.west);
            
        \end{tikzpicture}
    \end{figure}

\end{frame}

\end{document}

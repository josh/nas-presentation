#!/bin/bash
miktex-pdflatex -interaction nonstopmode -halt-on-error -file-line-error main.tex
rm -rf main.aux main.log main.nav main.snm main.out main.toc
mv main.pdf NAS_Survey.pdf
